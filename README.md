glapiplay: Experiments with the GitLab API
==========================================

Sourcing the activate script with `. ./activate` will enter a Python
virtualenv (creating it if necessary) with iPython and [python-gitlab]
installed. This allows you to use the [`gitlab` CLI tool] and the [API].


Command Line Setup
------------------

A sample `python-gitlab.cfg` is provided. This can be used directly
with the `-c python-gitlab.cfg` option or copied to the default
location, `~/.python-gitlab.cfg`.

To use it to connect to GitLab.com you'll need to set up a [personal
access token][pat], which you must be careful not to commit! You can
put the token in a second config file `secret.cfg` (which is ignored
by Git):

    [GitLab]
    private_token = VtBfEQjycy3SIBbp7bzM

This will override the `private_token` setting in `python-gitlab.cfg`
when you run commands like:

    gitlab -c python-gitlab.cfg  -c secret.cfg  current-user get



<!-------------------------------------------------------------------->
[API]: https://python-gitlab.readthedocs.io/en/stable/api-usage.html
[`gitlab` CLI tool]: https://python-gitlab.readthedocs.io/en/stable/cli.html
[pat]: https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html
[python-gitlab]: https://python-gitlab.readthedocs.io/
